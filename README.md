# Aplicacion web viewColors **(Front)** 🚀

Estas instrucciones te permitirán preparar tu ambiente y obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas. Primero necesitamos instalar los programas y herramientas necesarias para que el sistema pueda ejecutarse de manera local sin problemas, a continuacion los pasos a seguir.

* Desacargar e instalar **NodeJS 12 o 13** y se puede descargar desde su pagina web que dejo a continuacion:
```
https://nodejs.org/en/
```
Puedes chequear la version instalada con el siguiente comando:
```
npm -v
```
* Instalar **Angular CLI 8 o 9** con el siguiente comando desde tu terminal o consola de comandos:

```
npm i -g @angular/cli
```

* Hacer git clone de este repositorio en alguna carpeta donde desees guardarlo.


## Instalación 🔧

A continuacion un par de comandos que te indican lo que debes ejecutar para tener un entorno de desarrollo ejecutandose

* Una vez instalado necesitaremos tener en el proyectos las dependecnias y librerias usadas, para ello ejecutar el siguiente comando:
```
npm install
```
* luego para levantar nuestro proyecto debemos ejecutar el siguiente comando y la aplicacion comenzara a compilarse:
```
ng serve
```
Si cada paso anterior se ejecuto correctamente veras en tu terminal algo como lo siguiente:
```
** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **
i ｢wdm｣: Compiled successfully.
```
Esto indica que el proyecto esta corriendo en el puerto 4200 y la url que nos muestra es la que usaremos para copiar en nuestro navegador.

## Herramientas utilizadas 🛠️

Estas fueron las tecnologias para el desarrollo del proyecto:

* [Angular 8](https://angular.io/) - El framework web usado
* [Angular Material](https://material.angular.io/) - Framework de estilos
* [NodeJS](https://nodejs.org/es/)


## Autor ✒️

* **Yoneiker Plaza** - *Desarrollo Total*


---
⌨️ Creado por Yoneiker Plaza 🤓