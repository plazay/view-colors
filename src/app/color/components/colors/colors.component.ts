import { Component, OnInit } from '@angular/core';
import { ColorsService } from './../../../core/services/colors/colors.service';

@Component({
  selector: 'app-colors',
  templateUrl: './colors.component.html',
  styleUrls: ['./colors.component.css']
})
export class ColorsComponent implements OnInit {

  colors: [];

  constructor(
    private colorsService: ColorsService
  ) { }

  ngOnInit() {
    this.fetchColors();
  }

  clickColor(id: string) {
    // console.log('product');
    console.log(id);
  }

  fetchColors() {
    this.colorsService.getAllColors()
    .subscribe(colors => {
      console.log(colors);
      console.log('hola');
      // this.colors = colors;
    });
  }

}
