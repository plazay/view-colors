import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ColorComponent } from './components/color/color.component';
import { ColorsComponent } from './components/colors/colors.component';
import { MaterialModule } from './../material/material.module';


@NgModule({
  declarations: [
    ColorComponent,
    ColorsComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ]
})
export class ColorModule { }
