import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from './../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ColorsService {

  constructor(
    private http: HttpClient
  ) { }

  getAllColors() {
    return this.http.get<any>(`${environment.url_colors}`);
  }
}
